(defpackage aoc
  (:use :common-lisp))

(defun read-lines (filename)
  (with-open-file (in filename)
    (loop for line = (read-line in nil nil)
          while line
          collect (parse-integer line))))

(apply '+ (read-lines "input1"))

(defun circular! (items)
  (setf (cdr (last items)) items))

(defun double-sum (nums &optional (seen '()) (start 0))
  (let ((total (+ start (car nums)))
        (rest (cdr nums)))
    (if (member total seen)
        total
        (double-sum rest (cons total seen) total))))

(member 1 '(1 2))

(double-sum (read-lines "input1"))

(double-sum '( 1 -1 3) '(2) 0)

(apply '+ '( 1 -2 3))

(car '(1 2 3))

(cons 1 '(2 3))

(setq data (read-lines "input1"))
(defvar *counters* (make-hash-table))
(defmacro how-many (obj)
  `(values (gethash ,obj *counters* 0)))
(defun count-it (obj) (incf (how-many obj)))
(dolist (x (coerce "abcabca" 'list)) (count-it x))
(how-many #\a)

(defun char-count (string
                   &optional (counts (make-hash-table)))
  (if (eq (length string) 0)
      counts
    (let* ((ch (elt string 0))
           (elem (gethash ch counts 0)))
      (setf (gethash ch counts))
      (char-count (subseq string 1) counts))))


(defun has-two (string &optional (seen nil))
  (cond
   ((eq (length string) 0) nil)
   ((member (elt string 0) seen) (elt string 0))
   (t (has-two (subseq string 1) (cons (elt string 0) seen)))))


(defun has-three (string)
  (let ((seen-char (has-two string)))
    (if seen-char
        (has-two string (list seen-char))
      nil)))



(null nil)
(find #\a '(#\a 1 #\b 2) :test fn :key #'cdr)

(find #\d "here are some letters that can be looked at" :test 'char>)

(find-if #'oddp '(1 2 3 4 5) :end 3 :from-end t)

(setf (getf pl #\c) 2)
pl
